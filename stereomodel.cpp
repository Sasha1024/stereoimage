#include "stereomodel.h"
#include <fstream>

void StereoModel::saveToFile(const Data3d &data) const
{
    std::ofstream outfile ("test.bin",std::ofstream::binary);

    size_t size = data.arr.size();
    outfile.write((char*)&size, sizeof(size));

    size = data.arr.size()*sizeof(cv::Point3f);
    outfile.write ((char*)&data.arr[0],size);

    outfile.close();
}

Data3d StereoModel::convertTo3d(const Mat &strmp) const
{
    Data3d model;
    for( int i = 0; i < strmp.rows; i++ ) {
        for( int j = 0; j < strmp.cols; j++ ) {
            model.arr.push_back(cv::Point3f(i,j,float(strmp.at<uchar>(i,j)/2)));
        }
    }
    return model;
}

Mat StereoModel::calcStereoMap(const std::string &path) const
{
    Mat img = imread( path, CV_LOAD_IMAGE_GRAYSCALE );
    if( img.empty() ) {
        return Mat();
    }
    Mat integ;
    integral(img, integ);

    float searchWndw = 1.1f;

    int window = int(searchWndw*findOptimalWindow(img,integ));
    int samplesz = window / 15;

    Mat dmap16= doMagicEye(img, integ, window, samplesz);
    Mat dmap8;
    dmap16.convertTo(dmap8,CV_8UC1);
    cv::Mat invSrc =  cv::Scalar::all(255) - dmap8;
    Mat blur;
    cv::blur(invSrc, blur, Size(10,10));
    equalizeHist(blur,blur);
    return blur;
}

StereoModel::StereoModel()
{

}

int StereoModel::idxToSum(const int x1,const int x2, const int y, const Mat &integral) const
{
    int p3 = integral.at<uint>(y+1,x2+1);
    int p2 = integral.at<uint>(y+1,x1-1);
    int p1 = integral.at<uint>(y,x2+1);
    int p0 = integral.at<uint>(y,x1-1);
    return p3-p2-p1+p0;
}

int StereoModel::integralWindow(const int x1,const int y1,const int x2, const int y2, const Mat &integral)const
{
    int p3 = integral.at<uint>(y2,x2);
    int p2 = integral.at<uint>(y2,x1);
    int p1 = integral.at<uint>(y1,x2);
    int p0 = integral.at<uint>(y1,x1);
    return p3-p2-p1+p0;
}

int StereoModel::findOptimalWindow(const Mat &img, const Mat &integral,const int minSplit,const int maxSplit)const
{
    int maxWin = img.rows / minSplit;
    int minWin = img.cols / maxSplit;
    float min =  std::numeric_limits<float>::max();
    int id=0;
    for (int i=minWin; i<maxWin; i++) {
        int left  = integralWindow(0,0,  i,img.rows,integral);
        int right = integralWindow(0,i,2*i,img.rows,integral);
        float  val = float(abs(left-right)/i);
        if (val<min) {
            min = val;
            id= i;
        }
    }
    return id;
}

Mat StereoModel::doMagicEye(const Mat &img, const Mat &integral, const int window, const int samplesz)const
{
    Mat dmap = Mat::zeros(img.rows,img.cols-window, CV_16UC1);
    for (int vidx=1; vidx < img.rows-1; vidx++) {
        for (int hidx = 1; hidx<img.cols-window-1; hidx++) {
            int sample = idxToSum(hidx,hidx+samplesz,vidx,integral);
            ushort min =  std::numeric_limits<ushort>::max();
            for (int sidx=int(window*0.8); sidx<window-samplesz; sidx++) {
                ushort val = abs(sample-idxToSum(hidx+sidx,hidx+sidx+samplesz,vidx,integral));
                if (val<min) {
                    min = val;
                }
            }
            dmap.at<ushort>(vidx,hidx) =  min;
        }
    }
    return dmap;
}
