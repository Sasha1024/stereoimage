#ifndef CAMERA_H
#define CAMERA_H
#include <GL/gl.h>
#include "opencv2/imgproc.hpp"

const std::string WIN_NAME = "OpenGL";

class Camera {
    cv::Point lastpos,position;
    bool leftButton;
public :
    void rotate() const
    {
        if (leftButton) {
            cv::Point dir = getDir();
            glRotated(dir.x, 0, 1, 0);
            glRotated(dir.y, 1, 0, 0);
        }
    }
    cv::Point getDir() const
    {
        return position-lastpos;
    }
    Camera():leftButton(false),lastpos(),position()
    {
        setMouseCallback(WIN_NAME, mouseHandler, this);
    }
    void setPosition(const int x,const int y)
    {
        lastpos= position;
        position.x = x;
        position.y = y;
    }
    void setLeftButton(const bool lb)
    {
        leftButton =lb;
    }
    static void mouseHandler(int event, int x, int y, int, void* this_)
    {
        Camera *m = static_cast<Camera*>(this_);
        if  ( event == EVENT_LBUTTONDOWN ) {
            m->setLeftButton(true);
        }
        else if (event == EVENT_LBUTTONUP) {
            m->setLeftButton(false);
        }
        m->setPosition(x, y);
    }
};
#endif // CAMERA_H
