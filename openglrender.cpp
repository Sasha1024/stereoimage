#include "openglrender.h"
#include <fstream>
#include "camera.h"

const float OFFSET_X=200;
const float OFFSET_Y=300;
const float OFFSET_COLOR=230;

OpenGLRender::~OpenGLRender()
{
    setOpenGlDrawCallback(WIN_NAME, 0, 0);
    destroyAllWindows();
}

OpenGLRender::OpenGLRender(const int winWidth,const int winHeight)
{
    namedWindow(WIN_NAME, WINDOW_OPENGL);
    resizeWindow(WIN_NAME, winWidth, winHeight);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0, (double)winHeight / winWidth, 0.1, 1500.0);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslated(0,0,-1000);
    glPointSize(1);


}
bool OpenGLRender::loadFile()
{
    std::ifstream InFile;
    InFile.open("test.bin", std::ios::in | std::ios::binary);
    if (InFile.is_open()) {
        size_t size;
        InFile.read( (char*)&size, sizeof(size_t));
        data.arr.resize(size);
        InFile.read((char*)&data.arr[0], size*sizeof(cv::Point3f));
        InFile.close();
    }
    else {
        return false;
    }
    setOpenGlDrawCallback(WIN_NAME, draw, &data);
    return true;
}
void OpenGLRender::update() const
{
    Camera camera;
    while ((char)waitKey(40)!=27) {
        camera.rotate();
        updateWindow(WIN_NAME);
    }
}

void OpenGLRender::draw(void* userData)
{
    Data3d* data = static_cast< Data3d*>(userData);
    glBegin(GL_POINTS);
    for (const auto &point:data->arr) {
        glColor3f(point.x/OFFSET_COLOR,point.y/OFFSET_COLOR,point.z/OFFSET_COLOR);
        glVertex3f(point.x-OFFSET_X,point.y-OFFSET_Y,point.z);

    }
    glEnd();
}
