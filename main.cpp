#include "stereomodel.h"
#include "openglrender.h"

const int WIN_WIDTH  = 800;
const int WIN_HEIGHT = 640;

int main(int argc, char** argv)
{
    std::string imageName("3.jpg");
    if( argc > 1) {
        imageName = argv[1];
    }

    StereoModel st;
    Mat stereoMap = st.calcStereoMap(imageName);
    cv::imshow("image", stereoMap);
    if( stereoMap.empty() ) {
        std::cout <<  "Could not open or find the image" << std::endl ;
        return -1;
    }

    Data3d data =  st.convertTo3d(stereoMap);
    st.saveToFile(data);

    OpenGLRender render(WIN_WIDTH,WIN_HEIGHT);
    if (render.loadFile()) {
        render.update();
    }
}
