#ifndef STEREOMODEL_H
#define STEREOMODEL_H
#include <iostream>
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "utils.h"

using namespace cv;

class StereoModel {
    int idxToSum(const int x1,const int x2, const int y, const Mat& integral) const;
    int integralWindow(const int x1,const int y1,const int x2, const int y2, const Mat& integral) const;
    int findOptimalWindow(const Mat &img, const Mat &integral,const int minSplit=4,const int maxSplit=16) const;
    Mat doMagicEye(const Mat &img, const Mat &integral, const int window, const int samplesz) const;
public:
    StereoModel();
    Mat calcStereoMap(const std::string &path)const;
    Data3d convertTo3d(const Mat &strmp)const;
    void saveToFile(const Data3d &data)const;
};

#endif // STEREOMODEL_H
