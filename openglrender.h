#ifndef OPENGLRENDER_H
#define OPENGLRENDER_H
#include "opencv2/core/opengl.hpp"
#include <GL/gl.h>
#include <GL/glu.h>
#include "opencv2/highgui.hpp"
#include "utils.h"

using namespace cv;

class OpenGLRender {
    Data3d data;
public:
    OpenGLRender(const int winWidth,const int winHeight);
    bool loadFile();
    void update() const;
    static void draw(void* userData);
    ~OpenGLRender();
};

#endif // OPENGLRENDER_H
